﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OpenSubtitlesHandler;

namespace SuboKatevatiri
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        public bool successLogin = false;

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string username = textBoxUsername.Text;
            string password = textBoxPassword.Text;
            if (username == "" || password == "")
            {
                label4.Text = "Please enter your credentials !";
            }
            else
            {
                string language = "eng";
                IMethodResponse response = OpenSubtitles.LogIn(username, password, language);
                if (response.Status == "200 OK")
                {
                    successLogin = true;
                    this.Close();

                }
                else
                {
                    label4.Text = "Wrong username/password !";
                }
            }
           
        }
    }
}
