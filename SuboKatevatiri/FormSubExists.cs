﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SuboKatevatiri
{
    public partial class FormSubExists : Form
    {
        public FormSubExists()
        {
            InitializeComponent();
        }

        public bool overwrite = false;

        private void button1_Click(object sender, EventArgs e)
        {
            overwrite = true;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            overwrite = false;
            this.Close();
        }
    }
}
