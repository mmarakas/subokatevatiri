﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OpenSubtitlesHandler;
using System.IO;
using System.Diagnostics;
using System.Windows;
using System.Text.RegularExpressions;
using SuboKatevatiri.Properties;
using System.Threading;
using System.Threading.Tasks;

namespace SuboKatevatiri
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }
        
        string language = "eng";
        string movieHash, movieSize, videoFilePath, subFilePath;
        private int[] downloadsList;
        bool loggedIn = false;
        bool initialized = false;
        Dictionary<SubtitleSearchResult, double> subDict = new Dictionary<SubtitleSearchResult, double>();

        private void FormMain_Load(object sender, EventArgs e)
        {
            backgroundWorkerLogin.RunWorkerAsync();
            if (Settings.Default.SubtitleLanguage != null)
            {
                language = Settings.Default.SubtitleLanguage;
            }

            listViewResults.Columns.Add("Name");
            listViewResults.Columns.Add("Similarity");
            listViewResults.Columns.Add("Downloads");
            listViewResults.Columns.Add("User");
            listViewResults.Columns.Add("Comment");
            listViewResults.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            listViewResults.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

            imageComboLanguages.Items.Add(new ImageComboItem("English", 0));
            imageComboLanguages.Items.Add(new ImageComboItem("Greek", 1));
            if(language == "eng")
                imageComboLanguages.SelectedIndex = 0;
            else imageComboLanguages.SelectedIndex = 1;
        }

        #region Search Background Worker
        private void backgroundWorkerSearch_DoWork(object sender, DoWorkEventArgs e)
        {
            subDict.Clear();
            movieHash = Utilities.ComputeHash(videoFilePath);
            FileInfo info = new FileInfo(videoFilePath);
            movieSize = info.Length.ToString();

            (sender as BackgroundWorker).ReportProgress(15, null);

            string videoFile = videoFilePath.Substring(videoFilePath.LastIndexOf("\\")).Trim('\\');
            string videoFileName = videoFile.Substring(0, videoFile.LastIndexOf(".")).Trim('\\');
            List<SubtitleSearchParameters> parms = new List<SubtitleSearchParameters>();
            parms.Add(new SubtitleSearchParameters(language, movieHash, long.Parse(movieSize)));

            (sender as BackgroundWorker).ReportProgress(30, null);

            IMethodResponse result = OpenSubtitles.SearchSubtitles(parms.ToArray());

            (sender as BackgroundWorker).ReportProgress(50, null);

            if (result is MethodResponseSubtitleSearch)
            {
                foreach (SubtitleSearchResult sub in ((MethodResponseSubtitleSearch)result).Results)
                {
                    double similarity = SimilarityExtensions.CalculateSimilarity(sub.SubFileName.Substring(0, sub.SubFileName.LastIndexOf(".")).ToLower(), videoFileName.ToLower());
                    Regex regex = new Regex(@"s\d\de\d\d");
                    Match match = regex.Match(videoFileName.ToLower());
                    if (sub.SubFileName.ToLower().Contains(match.ToString()))
                        subDict.Add(sub, Math.Round(similarity, 2));
                    else continue;
                }
            }

            (sender as BackgroundWorker).ReportProgress(75, null);

            List<KeyValuePair<SubtitleSearchResult, double>> sortedList = subDict.ToList();
            sortedList.Sort((x, y) => y.Value.CompareTo(x.Value));
            subDict.Clear();
            subDict = sortedList.ToDictionary(x => x.Key, x => x.Value);

            (sender as BackgroundWorker).ReportProgress(100, null);
        }

        private void backgroundWorkerSearch_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (subDict.Count == 0)
            {
                labelMessages.Text = "No subtitles found in " + imageComboLanguages.SelectedItem.ToString() + ".";
            }
            else if (subDict.Count == 1)
            {
                labelMessages.Text = "Found 1 subtitle in " + imageComboLanguages.SelectedItem.ToString() + ".";
            }
            else if (subDict.Count > 1)
            {
                labelMessages.Text = "Found " + subDict.Count + " subtitles in " + imageComboLanguages.SelectedItem.ToString() + ".";
            }
            label1.Text = movieHash;
            foreach (var kv in subDict)
            {
                ListViewItem nameItem = new ListViewItem(kv.Key.SubFileName);
                ListViewItem.ListViewSubItem similarityItem = new ListViewItem.ListViewSubItem(nameItem, (kv.Value * 100).ToString() + "%");
                ListViewItem.ListViewSubItem downloadsItem = new ListViewItem.ListViewSubItem(nameItem, kv.Key.SubDownloadsCnt);
                ListViewItem.ListViewSubItem userItem = new ListViewItem.ListViewSubItem(nameItem, kv.Key.UserNickName);
                ListViewItem.ListViewSubItem commentItem = new ListViewItem.ListViewSubItem(nameItem, kv.Key.SubAuthorComment);

                nameItem.SubItems.Add(similarityItem);
                nameItem.SubItems.Add(downloadsItem);
                nameItem.SubItems.Add(userItem);
                nameItem.SubItems.Add(commentItem);
                listViewResults.Items.Add(nameItem);
            }
            listViewResults.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            listViewResults.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            Task.Factory.StartNew(() =>
            {
                Thread.Sleep(500);
                this.Invoke(new Action(() => toolStripProgressBar.Visible = false));
            });
        }

        private void backgroundWorkerSearch_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            toolStripProgressBar.Value = e.ProgressPercentage;
            labelMessages.Text = "Searching for subtitles...";
        }
        #endregion

        #region Login Background Worker

        private void backgroundWorkerLogin_DoWork(object sender, DoWorkEventArgs e)
        {
            string username = "", password = "", useragent = "suboKatevatiri";
            (sender as BackgroundWorker).ReportProgress(20, null);
            OpenSubtitles.SetUserAgent(useragent);
            (sender as BackgroundWorker).ReportProgress(60, null);
            IMethodResponse response = OpenSubtitles.LogIn(username, password, language);
            (sender as BackgroundWorker).ReportProgress(100, null);
        }

        private void backgroundWorkerLogin_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            labelMessages.Text = "";
            initialized = true;
            string[] args = Environment.GetCommandLineArgs();
            if (args.Length > 1)
            {
                videoFilePath = args[1];
                toolStripStatusLabelFile.Text = videoFilePath.Split('\\').Last();
                toolStripProgressBar.Visible = true;
                backgroundWorkerSearch.RunWorkerAsync();
            }
            else
            {
                videoFilePath = "C:\\Users\\Makis\\Desktop\\The.Walking.Dead.S06E03.720p.HDTV.x264-KILLERS.mkv";
                //videoFilePath = @"E:\The.Wolf.of.Wall.Street.2013.DVDScr.x264-HaM.m4v";
                //videoFilePath = @"E:\House.of.Cards.2013.S03.1080p.WEBRip.x264-2HD\House.of.Cards.2013.S03E06.1080p.WEBRip.x264-2HD\house.of.cards.2013.s03e06.1080p.webrip.x264-2hd.mkv";
                //toolStripStatusLabelFile.Text = videoFilePath.Split('\\').Last();
                //backgroundWorkerSearch.RunWorkerAsync();

                toolStripStatusLabelFile.Text = "-";
            }
            
            Task.Factory.StartNew(() =>
            {
                Thread.Sleep(500);
                this.Invoke(new Action(() => toolStripProgressBar.Visible = false));
            });
        }

        private void backgroundWorkerLogin_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            toolStripProgressBar.Value = e.ProgressPercentage;
            labelMessages.Text = "Initializing...";
        }
        #endregion

        private void downloadSubtitle()
        {
            IMethodResponse result = OpenSubtitles.DownloadSubtitles(downloadsList);

            if (result is MethodResponseSubtitleDownload)
            {
                foreach (SubtitleDownloadResult res in ((MethodResponseSubtitleDownload)result).Results)
                {
                    byte[] data = Convert.FromBase64String(res.Data);
                    byte[] target = Utilities.Decompress(new MemoryStream(data));

                    string videoFileLocation = videoFilePath.Substring(0, videoFilePath.LastIndexOf("\\"));
                    string videoFile = videoFilePath.Substring(videoFilePath.LastIndexOf("\\")).Trim('\\');
                    string videoFileName = videoFile.Substring(0, videoFile.LastIndexOf(".")).Trim('\\');

                    subFilePath = videoFileLocation + "\\" + videoFileName + ".srt";

                    if (File.Exists(subFilePath))
                    {
                        FormSubExists frm = new FormSubExists();
                        frm.ShowDialog(this);
                        if (frm.overwrite)
                        {
                            Stream stream = new FileStream(subFilePath, FileMode.Create, FileAccess.Write);
                            stream.Write(target, 0, target.Length);
                            stream.Close();
                            labelMessages.Text = "Subtitle downloaded successfully !";
                        }
                    }
                    else
                    {
                        Stream stream = new FileStream(subFilePath, FileMode.Create, FileAccess.Write);
                        stream.Write(target, 0, target.Length);
                        stream.Close();
                        labelMessages.Text = "Subtitle downloaded successfully !";
                    }
                }
            }
        }

        private void buttonDownloadPlay_Click(object sender, EventArgs e)
        {
            if (!checkInitialization() || !checkFileLoaded() || !checkSubSelected())
            {
                return;
            }

            SubtitleSearchResult sub;
            labelMessages.Text = "Downloading subtitle...";
            foreach (SubtitleSearchResult item in subDict.Keys)
            {
                if (item.SubFileName.Equals(listViewResults.SelectedItems[0].Text))
                {
                    sub = item;
                    break;
                }
            }
            downloadsList = new int[] { int.Parse(sub.IDSubtitleFile) };

            downloadSubtitle();
            openVideoFile();
            this.Close();
        }

        private void buttonDownload_Click(object sender, EventArgs e)
        {
            if (!checkInitialization() || !checkFileLoaded() || !checkSubSelected())
            {
                return;
            }

            SubtitleSearchResult sub;
            labelMessages.Text = "Downloading subtitle...";
            foreach (SubtitleSearchResult item in subDict.Keys)
            {
                if (item.SubFileName.Equals(listViewResults.SelectedItems[0].Text))
                {
                    sub = item;
                    break;
                }
            }
            downloadsList = new int[] { int.Parse(sub.IDSubtitleFile) };
            downloadSubtitle();
        }

        private void openVideoFile()
        {
            ProcessStartInfo start = new ProcessStartInfo();
            start.FileName = videoFilePath;
            //start.WindowStyle = ProcessWindowStyle.Hidden;
            //start.CreateNoWindow = true;
            Process proc = Process.Start(start);    
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (initialized)
            {
                OpenFileDialog op = new OpenFileDialog();
                if (op.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                {
                    videoFilePath = op.FileName;
                    toolStripStatusLabelFile.Text = videoFilePath.Split('\\').Last();
                    listViewResults.Items.Clear();
                    toolStripProgressBar.Visible = true;
                    backgroundWorkerSearch.RunWorkerAsync();
                }
            }
            else
            {
                labelMessages.Text = "Please wait for initialization to complete !";
            }
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FormAbout frm = new FormAbout();
            frm.ShowDialog(this);
        }

        private void uploadSubtitleFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.opensubtitles.org/en/upload");
        }

        private void imageCombo1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (!checkInitialization() || !checkFileLoaded())
            {
                return;
            }
            switch (imageComboLanguages.SelectedIndex)
            {
                case 0:
                    language = "eng";
                    break;
                case 1:
                    language = "ell";
                    break;
            }

            listViewResults.Items.Clear();
            toolStripProgressBar.Visible = true;
            backgroundWorkerSearch.RunWorkerAsync();
        }

        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Login frm = new Login();
            frm.ShowDialog(this);
            if (frm.successLogin)
            {
                loggedIn = true;
                labelMessages.Text = "Logged in successfully !";
            }
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (loggedIn)
            {
                IMethodResponse response = OpenSubtitles.LogOut();
                if (response.Status == "200 OK")
                {
                    labelMessages.Text = "Logged out successfully";
                }
            }
            else
            {
                labelMessages.Text = "You are not logged in !";
            }
            
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (imageComboLanguages.SelectedIndex == 0)
                Settings.Default.SubtitleLanguage = "eng";
            else if (imageComboLanguages.SelectedIndex == 1)
                Settings.Default.SubtitleLanguage = "ell";

            Settings.Default.Save();
        }

        private bool checkInitialization()
        {
            if (!initialized)
            {
                labelMessages.Text = "Please wait for initialization to complete !";
                return false;
            }
            else return true;
        }

        private bool checkSubSelected()
        {
            if (listViewResults.SelectedItems.Count == 0)
            {
                labelMessages.Text = "Please select a subtitle !";
                return false;
            }
            else return true;
        }

        private bool checkFileLoaded()
        {
            if (videoFilePath == null)
            {
                labelMessages.Text = "Please open a video file !";
                return false;
            }
            else return true;
        }
    }
}
