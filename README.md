# SUBTITLE DOWNLOADER #

This is a simple subtitle downloader program created in C#. It uses the Opensubtitles.org API through the OpenSubtitlesHandler library that implement all OpenSubtitles.org XML-RPC methods.

## Contact ##

mmarakas (at) gmail (dot) com